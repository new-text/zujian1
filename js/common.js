$(function(){
    // *********************************
    //导航高亮显示效果
    // *********************************
    function HighLight(opts){
        this.hoverEl = opts.hoverEl;
        this.sonNav = opts.sonNav;
        this.box = opts.box;
        this.header = opts.header;
        this.menu = opts.menu;
        this.curClass = opts.curClass;
    }
    HighLight.prototype = {
        _slideDownFun: function(_obj,_class){
            var e = this;
            _obj.hover(function(){
                $(this).find(_class).stop(true,false).slideDown();
            },function(){
                $(this).find(_class).hide();
            });   
        },
        _scrollTop: function(_obj){
            var e = this,
                _arr = [],
                _winSrc = $(window).scrollTop(),
                _len = _obj.length;
            _obj.each(function(){
                var _top = $(this).offset().top-90;
                _arr.push(_top);
            });
            e.header.each(function(){
                var _obj = $(this);
                _obj.find(e.hoverEl).children('a').on('click',function(){
                    var k = $(this).parent().index();
                    $('html,body').animate({
                        scrollTop:_arr[k]
                    },500);
                    if($(this).hasClass('huanan-position')){
                        $(this).addClass(e.curClass).parent().siblings().children().removeClass(e.curClass);
                    }
                });
                e._scrollFun(_len,_winSrc,_arr,_obj,true);
                $(window).on('scroll',function(){
                    _winSrc = $(window).scrollTop();
                    e._scrollFun(_len,_winSrc,_arr,_obj,true);
                });
            });
        },
        _scrollFun: function(_len,_winSrc,_arr,_obj,_flag){
            var e = this;
            if(_flag){
                for(var i=0; i<_len; i++){
                    if(_arr[i] <= _winSrc + 100){
                       _obj.find(e.hoverEl).eq(i).children().addClass(e.curClass).parent().siblings().children().removeClass(e.curClass);
                    }
                }
            }
            else{
                for(var i=0; i<_len; i++){
                    if(_arr[i] <= _winSrc + 600){
                        _obj.eq(i).addClass(e.curClass);
                    }
                }
            }
            
        },
        _flowMenu: function(){
            var e = this,
                winScr = $(window).scrollTop();
            function checkFun(){
                if(winScr > 140){
                    e.menu.next().fadeIn();
                }
                else{
                    e.menu.next().fadeOut();
                }
            }
            checkFun();
            $(window).bind('scroll',function(){
                winScr = $(window).scrollTop();
                checkFun();
            });
        },
        _init: function(){
            var e = this;
            e._slideDownFun($(e.hoverEl),e.sonNav);
            e._scrollTop(e.box);
            e._flowMenu();
        }
    };
    new HighLight({
        hoverEl:'.chuang-nav > ul >li',
        sonNav: '.chuang-son-nav',
        box: $('.box'),
        header: $('.chuang-header'),
        menu: $('.chuang-index-header'),
        curClass: 'active'
    })._init();
    (function(){
        //返回顶部
        $('<div id="top"></div>').appendTo($("body"));
        $("#top").click(function(){
            $("html,body").animate({scrollTop:0},200);
        })
        var navBox=$(".home-header .nav");          
        $(window).bind("scroll", function(){
               var tops=parseInt($(window).scrollTop());
               //console.log(tops);
               if(tops==0||tops<0){
                $("#top").css({display:"none"});
               }else{
                $("#top").css({display:"block"});  
               }
        })

        //返回顶部
        var Li=$(".ecology .list ul li").hover(function(){
            $(this).addClass('on').siblings().removeClass('on');
        },function(){

        })

        //数字动画
        var panic=$(".panic-buying");
        var panic_Top=panic.offset().top;
        var panic_T=panic.find(".effects-num");
        var panic_TX=panic_T.text();
        panic_T.text(0);
        var ii_flag=true,
        dd=false;
        $(window).bind("scroll",function(){
             var scroll_Tops=$(window).scrollTop();
             if(dd) return;
             if(scroll_Tops>panic_Top){

                if(!ii_flag) return;
                ii_flag=false;
                var ii=0;
                setInterval(function(){
                  ii++; 
                  if(ii>panic_TX){
                    ii=panic_TX;
                    ii_flag=true;
                    dd=true;
                  }
                  panic_T.text(ii);
                },5)
                
                
             }
        })     

    })();
});